# Code to execute in an independent thread
import time
from threading import Thread


def countdown(n):
    while n > 0:
        print("T-minus", n)
        n -= 1
        time.sleep(5)


# Create and launch a thread
t = Thread(target=countdown, args=(10,))
t.start()
